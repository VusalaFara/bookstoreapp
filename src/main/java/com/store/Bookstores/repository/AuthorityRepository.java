package com.store.Bookstores.repository;
import com.store.Bookstores.model.Authorities;
import com.store.Bookstores.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface AuthorityRepository extends JpaRepository<Authority,Long> {

    Optional<Authority> findByAuthorities(Authorities authority);
}

package com.store.Bookstores.repository;
import com.store.Bookstores.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface BookRepository extends JpaRepository<Book,Long> {
     Optional<Book> findBookByNameIgnoreCase(String name);
     Optional<Book> findBookByName(String name);

}

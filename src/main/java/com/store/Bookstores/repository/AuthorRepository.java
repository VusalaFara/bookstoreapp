package com.store.Bookstores.repository;
import com.store.Bookstores.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorRepository extends JpaRepository<Author,Long> {

    Author findAuthorByName(String name);


    Optional<Author> findByEmail(String name);
}

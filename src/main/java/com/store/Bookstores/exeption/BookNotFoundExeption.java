package com.store.Bookstores.exeption;

public class BookNotFoundExeption extends Exception{
    public BookNotFoundExeption(String message) {
        super(message);
    }
}

package com.store.Bookstores.exeption;

public class NotFoundExeption extends Exception{

    public NotFoundExeption(String message){
        super(message);
    }
}

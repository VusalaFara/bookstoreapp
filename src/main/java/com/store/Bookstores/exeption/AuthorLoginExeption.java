package com.store.Bookstores.exeption;

public class AuthorLoginExeption extends Exception{

    public AuthorLoginExeption(String message){
        super(message);
    }
}

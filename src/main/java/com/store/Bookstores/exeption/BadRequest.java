package com.store.Bookstores.exeption;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class BadRequest extends RuntimeException{
    private final ErrorField errorField;

   public BadRequest(String message, ErrorField errorField){
       super(message);
       this.errorField = errorField;
   }
}

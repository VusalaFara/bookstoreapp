package com.store.Bookstores.exeption;

public class BookExsistsExeption extends Exception{

    public BookExsistsExeption(String message){
        super(message);
    }
}

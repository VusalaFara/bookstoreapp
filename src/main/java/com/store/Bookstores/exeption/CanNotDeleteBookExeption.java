package com.store.Bookstores.exeption;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class CanNotDeleteBookExeption extends RuntimeException{

    public CanNotDeleteBookExeption (String message){
        super(message);
    }
}

package com.store.Bookstores.controller;

import com.store.Bookstores.dto.BookResponse;
import com.store.Bookstores.dto.StudentResponse;
import com.store.Bookstores.exeption.NotFoundExeption;
import com.store.Bookstores.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @GetMapping("/{bookName}")
    public ResponseEntity<List<StudentResponse>> getReadersOfSpecificBook(@PathVariable String bookName) throws NotFoundExeption {
        return new ResponseEntity<>(bookService.getReadersOfSpecificBook(bookName), HttpStatus.OK);
    }
}

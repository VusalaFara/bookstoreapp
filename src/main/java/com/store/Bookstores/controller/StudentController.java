package com.store.Bookstores.controller;
import com.store.Bookstores.dto.AuthorResponse;
import com.store.Bookstores.dto.BookResponse;
import com.store.Bookstores.dto.StudentRequest;
import com.store.Bookstores.dto.StudentResponse;
import com.store.Bookstores.exeption.NotFoundExeption;
import com.store.Bookstores.exeption.BookNotFoundExeption;
import com.store.Bookstores.exeption.UserLoginExeption;
import com.store.Bookstores.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService userService;


    @GetMapping("/currentlyRead/{id}")
    public ResponseEntity<List<BookResponse>> getBooks(@PathVariable Long id) throws NotFoundExeption {
        return new ResponseEntity<>(userService.getBooks(id), HttpStatus.OK);
    }//new

    @GetMapping("/{id}/{bookName}")
    public ResponseEntity<BookResponse> getSpecificBook(@PathVariable Long id, @PathVariable String bookName) throws NotFoundExeption, BookNotFoundExeption {
        return new ResponseEntity<>(userService.getSpecificBook(id, bookName), HttpStatus.OK);
    }

    @PostMapping("/subscribe/{studentId}/{authorId}")
    public ResponseEntity<String> subscribe(@PathVariable Long studentId, @PathVariable Long authorId) throws NotFoundExeption {
        userService.subscribe(studentId, authorId);
        return new ResponseEntity<>("Successfully subscribed!", HttpStatus.OK);
    }

}


package com.store.Bookstores.controller;
import com.store.Bookstores.dto.*;
import com.store.Bookstores.exeption.AuthorLoginExeption;
import com.store.Bookstores.exeption.NotFoundExeption;
import com.store.Bookstores.exeption.BookExsistsExeption;
import com.store.Bookstores.exeption.BookNotFoundExeption;
import com.store.Bookstores.model.Author;
import com.store.Bookstores.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping("/author")
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService authorService;

//    @PostMapping("/login")
//    public ResponseEntity<Author> loginAuthor(@RequestBody LoginRequest loginRequest) throws AuthorLoginExeption {
//        return new ResponseEntity<>(authorService.loginAuthor(loginRequest), HttpStatus.OK);
//    }

    @GetMapping("/book/{id}")
    public ResponseEntity<List<BookResponse>> getBooks(@PathVariable Long id) throws AuthorLoginExeption, NotFoundExeption {
        ;
        return new ResponseEntity<>(authorService.getBooks(id), HttpStatus.OK);
    }

    @PostMapping("/{id}")
    public ResponseEntity<AuthorResponse> saveAuthorsBook(@PathVariable Long id, @RequestBody BookRequest bookRequest) throws AuthorLoginExeption, NotFoundExeption, BookExsistsExeption, BookExsistsExeption {
        return new ResponseEntity<>(authorService.saveAuthorsBook(id, bookRequest), HttpStatus.OK);
    }//new


    @DeleteMapping("/{authorId}/{bookId}")
    public ResponseEntity<Long> deleteBook(@PathVariable Long authorId, @PathVariable Long bookId) throws BookNotFoundExeption, NotFoundExeption, AuthorLoginExeption {
        return new ResponseEntity<>(authorService.deleteAuthorsBook(authorId, bookId), HttpStatus.OK);
    }
}//new



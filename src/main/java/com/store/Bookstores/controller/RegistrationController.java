package com.store.Bookstores.controller;
import com.store.Bookstores.dto.JwtResponse;
import com.store.Bookstores.dto.LoginRequest;
import com.store.Bookstores.dto.RegisterRequest;
import com.store.Bookstores.dto.RegisterResponse;
import com.store.Bookstores.exeption.AuthorLoginExeption;
import com.store.Bookstores.exeption.NotFoundExeption;
import com.store.Bookstores.service.RegisterService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@Slf4j
public class RegistrationController {

    private final RegisterService registerService;


    @PostMapping("/register")
    public ResponseEntity<RegisterResponse> register(@RequestBody RegisterRequest registerRequest) throws AuthorLoginExeption {
        return new ResponseEntity<>(registerService.registerUser(registerRequest), HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<JwtResponse> login(@RequestBody LoginRequest loginRequest) throws AuthorLoginExeption, NotFoundExeption {
        return new ResponseEntity<>(registerService.login(loginRequest), HttpStatus.OK);
    }
}


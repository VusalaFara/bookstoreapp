package com.store.Bookstores.dto;
import com.store.Bookstores.model.Authority;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class StudentRequest {

    String name;
    Long age;
    List<Authority>authoritiesList;
}

package com.store.Bookstores.dto;
import com.store.Bookstores.model.Authority;
import com.store.Bookstores.model.Book;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class AuthorRequest {

    String name;
    Long age;
    List<Book>books;
    List<Authority>authoritiesList;


}

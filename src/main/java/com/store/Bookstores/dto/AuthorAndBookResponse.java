package com.store.Bookstores.dto;
import com.store.Bookstores.model.Book;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class AuthorAndBookResponse {

    String name;
    String surname;
    String email;
    String password;
    List<Book> books;
}

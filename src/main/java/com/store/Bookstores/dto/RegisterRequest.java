package com.store.Bookstores.dto;

import com.store.Bookstores.model.Authority;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class RegisterRequest {

    String name;
    Long age;
    String password;
    String email;
    String userType;
    List<Authority> authorityList;
}

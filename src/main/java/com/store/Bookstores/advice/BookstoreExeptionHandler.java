package com.store.Bookstores.advice;
import com.store.Bookstores.exeption.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestControllerAdvice
public class BookstoreExeptionHandler {


    @ExceptionHandler(BadRequest.class)
    public ResponseEntity<ErrorResponse> handleBadRequestExeption(BadRequest ex, WebRequest request) {
        Map<String, String> errorFields = new HashMap<>();
        errorFields.put("error message", ex.getErrorField().name());
        String requestPath = ((ServletWebRequest) request).getRequest().getRequestURI();
        ErrorResponse errorResponse = ErrorResponse.builder()
                .issuedAt(LocalDateTime.now())
                .statusCode(201)
                .message(errorFields)
                .path(requestPath)
                .build();
        return new ResponseEntity<>(errorResponse, BAD_REQUEST);
    }
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorResponse> handleSamePasswordExeption(RuntimeException ex, WebRequest request) {
        Map<String, String> errorFields = new HashMap<>();
        errorFields.put("error message", ex.getMessage());
        String requestPath = ((ServletWebRequest) request).getRequest().getRequestURI();
        ErrorResponse errorResponse = ErrorResponse.builder()
                .issuedAt(LocalDateTime.now())
                .statusCode(201)
                .message(errorFields)
                .path(requestPath)
                .build();
        return new ResponseEntity<>(errorResponse, BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(AuthorLoginExeption.class)
    public Map<String,String> handleAuthorLoginExeption(AuthorLoginExeption ex){
        Map<String,String>errorMap = new HashMap<>();
        errorMap.put("error message: ", ex.getMessage());
        return errorMap;
    }
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(NotFoundExeption.class)
    public Map<String,String> handleAuthorNotFoundExeption(NotFoundExeption ex){
        Map<String,String>errorMap = new HashMap<>();
        errorMap.put("error message: ", ex.getMessage());
        return errorMap;
    }
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(BookExsistsExeption.class)
    public Map<String,String> handleBookExsistsExeption(BookExsistsExeption ex){
        Map<String,String>errorMap = new HashMap<>();
        errorMap.put("error message: ", ex.getMessage());
        return errorMap;
    }
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(BookNotFoundExeption.class)
    public Map<String,String> handleBookNotFoundExeption(BookNotFoundExeption ex){
        Map<String,String>errorMap = new HashMap<>();
        errorMap.put("error message: ", ex.getMessage());
        return errorMap;
    }
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(UserLoginExeption.class)
    public Map<String,String> handleUserLoginExeption(UserLoginExeption ex){
        Map<String,String>errorMap = new HashMap<>();
        errorMap.put("error message: ", ex.getMessage());
        return errorMap;
    }

}

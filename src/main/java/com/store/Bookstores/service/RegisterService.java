package com.store.Bookstores.service;

import com.store.Bookstores.dto.JwtResponse;
import com.store.Bookstores.dto.LoginRequest;
import com.store.Bookstores.dto.RegisterRequest;
import com.store.Bookstores.dto.RegisterResponse;
import com.store.Bookstores.exeption.NotFoundExeption;

public interface RegisterService {

    RegisterResponse registerUser(RegisterRequest registerRequest);

    JwtResponse login(LoginRequest loginRequest) throws NotFoundExeption;
}

package com.store.Bookstores.service;


import com.store.Bookstores.dto.BookRequest;
import com.store.Bookstores.dto.BookResponse;
import com.store.Bookstores.dto.StudentResponse;
import com.store.Bookstores.exeption.BookExsistsExeption;
import com.store.Bookstores.exeption.BookNotFoundExeption;
import com.store.Bookstores.exeption.NotFoundExeption;
import com.store.Bookstores.model.Book;

import java.util.List;
import java.util.Optional;

public interface BookService {

    Book saveBook(BookRequest bookRequest) throws BookExsistsExeption;

    Optional<Book> getBookByİd(Long id);

    List<BookResponse> getBooks();

    Book updateBook(BookRequest bookRequest, Long id) throws BookNotFoundExeption;

    Long deleteBook(Long id);

    List<StudentResponse> getReadersOfSpecificBook(String bookName) throws NotFoundExeption;
}

package com.store.Bookstores.service;

import com.store.Bookstores.dto.*;
import com.store.Bookstores.exeption.AuthorLoginExeption;
import com.store.Bookstores.exeption.NotFoundExeption;
import com.store.Bookstores.exeption.BookExsistsExeption;
import com.store.Bookstores.model.Author;

import java.util.List;

public interface AuthorService {


    List<BookResponse> getBooks(Long id) throws AuthorLoginExeption, NotFoundExeption;

    Long deleteAuthorsBook(Long id, Long bookId) throws NotFoundExeption, AuthorLoginExeption;
    public AuthorResponse saveAuthorsBook(Long id, BookRequest bookRequest) throws AuthorLoginExeption, NotFoundExeption, BookExsistsExeption;

}

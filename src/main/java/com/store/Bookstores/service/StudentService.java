package com.store.Bookstores.service;
import com.store.Bookstores.dto.*;
import com.store.Bookstores.exeption.NotFoundExeption;
import com.store.Bookstores.exeption.BookNotFoundExeption;
import com.store.Bookstores.exeption.UserLoginExeption;
import com.store.Bookstores.model.Student;

import java.util.List;

public interface StudentService {
    List<BookResponse> getBooks(Long id) throws NotFoundExeption;

//    List<Student> getUsers();

    BookResponse getSpecificBook(Long id, String bookName) throws NotFoundExeption, BookNotFoundExeption;

    List<AuthorResponse> subscribe(Long studentId, Long authorId) throws NotFoundExeption;
}

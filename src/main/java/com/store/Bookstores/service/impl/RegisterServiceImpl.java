package com.store.Bookstores.service.impl;

import com.store.Bookstores.config.JwtService;
import com.store.Bookstores.dto.*;
import com.store.Bookstores.exeption.BadRequest;
import com.store.Bookstores.exeption.ErrorField;
import com.store.Bookstores.exeption.NotFoundExeption;
import com.store.Bookstores.model.*;
import com.store.Bookstores.repository.AuthorRepository;
import com.store.Bookstores.repository.AuthorityRepository;
import com.store.Bookstores.repository.StudentRepository;
import com.store.Bookstores.repository.UserRepository;
import com.store.Bookstores.service.RegisterService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class RegisterServiceImpl implements RegisterService {
    private final UserRepository userRepository;
    private final AuthorRepository authorRepository;
    private final StudentRepository studentRepository;
    private final JwtService jwtService;
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;
    private final AuthorityRepository authorityRepository;

    @Override
    public RegisterResponse registerUser(RegisterRequest registerRequest) {
        List<Authorities> inputAuthorities = registerRequest.getAuthorityList().stream()
                .map(authority -> authority.getAuthorities()).collect(Collectors.toList());
        List<Authority> authorities = retrieveOrCreateAuthorities(inputAuthorities);
        if (userRepository.findByEmail(registerRequest.getEmail()).isPresent()) {
            throw new BadRequest(ErrorField.USER_ALREADY_EXSISTS);
        }

            AuthUser authUser = AuthUser.builder()
                    .name(registerRequest.getName())
                    .password(passwordEncoder.encode(registerRequest.getPassword()))
                    .userType(UserType.valueOf(registerRequest.getUserType()))
                    .email(registerRequest.getEmail())
                    .authorityList(authorities)
                    .build();

            if (UserType.STUDENT.equals(authUser.getUserType())) {
                if (studentRepository.findByEmail(registerRequest.getEmail()).isPresent()) {
                    throw new BadRequest(ErrorField.USER_ALREADY_EXSISTS);
                }

                Student student = Student.builder()
                        .name(registerRequest.getName())
                        .age(registerRequest.getAge())
                        .password(passwordEncoder.encode(registerRequest.getPassword()))
                        .email(registerRequest.getEmail())
                        .authoritiesList(authorities)
                        .build();
                studentRepository.save(student);

            } else if (UserType.AUTHOR.equals(authUser.getUserType())) {
                if (authorRepository.findByEmail(registerRequest.getEmail()).isPresent()) {
                    throw new BadRequest(ErrorField.USER_ALREADY_EXSISTS);
                }

                Author author = Author.builder()
                        .name(registerRequest.getName())
                        .age(registerRequest.getAge())
                        .password(passwordEncoder.encode(registerRequest.getPassword()))
                        .email(registerRequest.getEmail())
                        .authoritiesList(authorities)
                        .build();
                authorRepository.save(author);
            }

            AuthUser save = userRepository.save(authUser);
       return RegisterResponse.builder()
               .name(authUser.getName())
               .email(authUser.getEmail())
               .password(authUser.getPassword())
               .userType(authUser.getUserType().name())
               .build();
                }
    private List<Authority> retrieveOrCreateAuthorities(List<Authorities> inputAuthorities) {
        List<Authority> authorities = new ArrayList<>();
        for (Authorities inputAuthority : inputAuthorities) {
            Optional<Authority> optionalAuthority =
                    authorityRepository.findByAuthorities(inputAuthority);
            if (optionalAuthority.isPresent()) {
                authorities.add(optionalAuthority.get());
            } else {
                Authority newAuthority = Authority.builder()
                        .authorities(inputAuthority)
                        .build();
                authorityRepository.save(newAuthority);
                authorities.add(newAuthority);
            }
        }
        return authorities;
    }


    @Override
    public JwtResponse login(LoginRequest loginRequest) throws NotFoundExeption {
        AuthUser authUser = userRepository.findByEmail(loginRequest.getUsername())
                .orElseThrow(()->new NotFoundExeption("User not found"));
        log.info("user"+ authUser.getEmail());
       if(!passwordEncoder.matches(loginRequest.getPassword(), authUser.getPassword())) {
           throw new RuntimeException("Invalid username or password");
       }
            String token = jwtService.generateToken(authUser);
            return JwtResponse.builder()
                    .jwt(token)
                    .build();
    }
}



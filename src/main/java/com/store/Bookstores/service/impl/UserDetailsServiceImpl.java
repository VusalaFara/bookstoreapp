package com.store.Bookstores.service.impl;
import com.store.Bookstores.model.MyUserPrincipal;
import com.store.Bookstores.model.AuthUser;
import com.store.Bookstores.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
//should implement userdetailservice
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;


    @Override
    @SneakyThrows
//    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AuthUser authUser = userRepository.findByEmail(username).orElseThrow(() -> new RuntimeException("user not found!"));
        return new MyUserPrincipal(authUser);
//            return org.springframework.security.core.userdetails.User.builder()
//                    .username(user.getName())
//                    .authorities(user.getAuthorityList().stream().map(authority ->
//                            new SimpleGrantedAuthority(authority.getAuthorities().name())).collect(Collectors.toSet()))
//                    .build();
    }



//    @Override
//    @SneakyThrows
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        Optional<Author> author = authorRepository.findByEmail(username);
//        Optional<LoginUser> loginUser = loginUserRepository.findByEmail(username);
//
//        if (author.isPresent()) {
//            return buildUserDetails(author.get());
//        } else if (loginUser.isPresent()) {
//            return buildUserDetails(loginUser.get());
//        } else {
//            throw new UsernameNotFoundException("User not found");
//        }
//    }
//
//    private UserDetails buildUserDetails(Author author) {
//        return User.builder()
//                .username(author.getEmail())
//                .password(author.getPassword())
//                .authorities(author.getAuthoritiesList().stream()
//                        .map(authority -> new SimpleGrantedAuthority(authority.getAuthorities().name()))
//                        .collect(Collectors.toSet()))
//                .build();
//    }
//
//    private UserDetails buildUserDetails(LoginUser loginUser) {
//        return User.builder()
//                .username(loginUser.getEmail())
//                .password(loginUser.getPassword())
//                .authorities(loginUser.getAuthoritiesList().stream()
//                        .map(authority -> new SimpleGrantedAuthority(authority.getAuthorities().name()))
//                        .collect(Collectors.toSet()))
//                .build();
//    }
}



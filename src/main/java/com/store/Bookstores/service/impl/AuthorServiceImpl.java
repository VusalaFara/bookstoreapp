package com.store.Bookstores.service.impl;

import com.store.Bookstores.dto.AuthorResponse;
import com.store.Bookstores.dto.BookRequest;
import com.store.Bookstores.dto.BookResponse;
import com.store.Bookstores.exeption.*;
import com.store.Bookstores.model.Author;
import com.store.Bookstores.model.Book;
import com.store.Bookstores.repository.AuthorRepository;
import com.store.Bookstores.repository.BookRepository;
import com.store.Bookstores.service.AuthorService;
import com.store.Bookstores.service.BookService;
import com.store.Bookstores.service.EmailService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository repository;
    private final BookService bookService;
    private final BookRepository bookRepository;
    private final ModelMapper mapper;
//    private final PasswordEncoder passwordEncoder;
    private final EmailService emailService;



    @Override
    public List<BookResponse> getBooks(Long id) throws AuthorLoginExeption, NotFoundExeption {
        Author author = repository.findById(id).orElseThrow(() ->
                new NotFoundExeption("author with id: " + id + " not found"));
        log.info("author issss  " + author);
        return author.getBooks().stream().map(book -> mapper.map(book, BookResponse.class)).collect(Collectors.toList());

    }

    @Override
    @Transactional
    public AuthorResponse saveAuthorsBook(Long id, BookRequest bookRequest) throws AuthorLoginExeption, NotFoundExeption, BookExsistsExeption {
        Author author = repository.findById(id).orElseThrow(() ->
                new NotFoundExeption("author with id: " + id + " not found"));
        log.info("Author ++++++++++++++" + author);
        if (!author.getBooks().stream()
                .anyMatch(book -> book.getName().equals(bookRequest.getName()))) {
            Book newBook = Book.builder()
                    .name(bookRequest.getName())
                    .build();
            author.getBooks().add(newBook);
            newBook.setAuthor(author);
            author.getStudents().stream().forEach(student ->
                    emailService.sendEmail(student.getEmail(),"New book "+ bookRequest.getName(),"Dear "+student.getName()+
                            " a new book has been published by "+ author.getName()+". Best Regards!"));

            repository.save(author);
            return AuthorResponse.builder()
                    .name(author.getName())
                    .bookName(newBook.getName())
                    .build();
        } else {
            throw new BookExsistsExeption("A book with the same name already exists for this author.");
        }
    }

    @Override
    public Long deleteAuthorsBook(Long id, Long bookId) throws NotFoundExeption, AuthorLoginExeption {
        Author author = repository.findById(id).orElseThrow(() ->
                new NotFoundExeption("author with id: " + id + " not found"));
        Book book = bookRepository.findById(bookId).orElseThrow(() ->
                new NotFoundExeption("Bood with id: " + id + " not found"));
        if (!book.getStudents().isEmpty()) {
            throw new RuntimeException("Cannot delete the book because it is still referenced by students.");
        }
        author.getBooks().removeIf(book1 -> book1.getId().equals(bookId));
        bookService.deleteBook(bookId);
        repository.save(author);
        return bookId;
    }
}


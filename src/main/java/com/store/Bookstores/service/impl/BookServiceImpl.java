package com.store.Bookstores.service.impl;
import com.store.Bookstores.dto.BookRequest;
import com.store.Bookstores.dto.BookResponse;
import com.store.Bookstores.dto.StudentResponse;
import com.store.Bookstores.exeption.BookExsistsExeption;
import com.store.Bookstores.exeption.BookNotFoundExeption;
import com.store.Bookstores.exeption.NotFoundExeption;
import com.store.Bookstores.model.Book;
import com.store.Bookstores.repository.BookRepository;
import com.store.Bookstores.service.BookService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final ModelMapper mapper;


    @Override
    public Book saveBook(BookRequest bookRequest) throws BookExsistsExeption {
        if (bookRepository.findBookByName(bookRequest.getName()) == null) {
            Book book = mapper.map(bookRequest, Book.class);
            return bookRepository.save(book);
        } else {
            throw new BookExsistsExeption("Book with name " + bookRequest.getName() + " already exsists");
        }

    }

    @Override
    public Optional<Book> getBookByİd(Long id) {
        return bookRepository.findById(id);
    }

    @Override
    public List<BookResponse> getBooks() {
        return bookRepository.findAll().stream().map(book -> mapper.map(book, BookResponse.class)).toList();
    }

    @Override
    public Book updateBook(BookRequest bookRequest, Long id) throws BookNotFoundExeption {
        if (bookRepository.findById(id).isPresent()) {
            Book book = bookRepository.findById(id).get();
            book.setName(bookRequest.getName());
            return bookRepository.save(book);
        } else {
            throw new BookNotFoundExeption("book with id: " + id + " not found");
        }
    }

    @Override
    public Long deleteBook(Long id)  {
        bookRepository.deleteById(id);
            return id;
        }

    @Override
    public List<StudentResponse> getReadersOfSpecificBook(String bookName) throws NotFoundExeption {
        Book book = bookRepository.findBookByNameIgnoreCase(bookName).orElseThrow(() ->
                new NotFoundExeption("Bood with name: " + bookName + " not found"));
        return book.getStudents().stream().map(student -> mapper.map(student,StudentResponse.class))
                .collect(Collectors.toList());

    }//new
    }



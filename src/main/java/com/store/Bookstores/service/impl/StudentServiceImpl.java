package com.store.Bookstores.service.impl;
import com.store.Bookstores.dto.*;
import com.store.Bookstores.exeption.*;
import com.store.Bookstores.model.*;
import com.store.Bookstores.repository.AuthorRepository;
import com.store.Bookstores.repository.BookRepository;
import com.store.Bookstores.repository.StudentRepository;
import com.store.Bookstores.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final ModelMapper modelMapper;
//    private final PasswordEncoder passwordEncoder;


    @Override
    public List<BookResponse> getBooks(Long id) throws NotFoundExeption {
        Student student = studentRepository.findById(id).orElseThrow(() ->
                new NotFoundExeption("Student with id: " + id + " not found"));
            return bookRepository.findAll().stream().map
                    (book -> modelMapper.map(book, BookResponse.class)).collect(Collectors.toList());

    }

    @Override
    public BookResponse getSpecificBook(Long id, String bookName) throws NotFoundExeption, BookNotFoundExeption {
        Student student = studentRepository.findById(id).orElseThrow(() ->
                new NotFoundExeption("user with id: " + id + " not found"));
        if (bookName!= null && bookRepository.findBookByName(bookName).isPresent()) {
            Book book = bookRepository.findBookByName(bookName).get();
            log.info("book is"+book.getName());
            student.getBooks().add(book);
            //an api to retrieve a list of readers for a specific book
//        book.getStudents().add(student);
            studentRepository.save(student);
            return modelMapper.map(book, BookResponse.class);
        } else
            throw new BookNotFoundExeption("Book with name : " + bookName + " not found");

    }

    @Override
    public List<AuthorResponse> subscribe(Long studentId, Long authorId) throws NotFoundExeption {
        Student student = studentRepository.findById(studentId).orElseThrow(() ->
                new NotFoundExeption("Student with id: " + studentId + " not found"));
        log.info("Student is "+ student.getName());
        Author author = authorRepository.findById(authorId).orElseThrow(() ->
                new NotFoundExeption("Author with id: " + authorId + " not found"));
        if(student != null && author!=null){
            student.getAuthors().add(author);
            studentRepository.save(student);
            return student.getAuthors().stream().map(author1 ->
                    modelMapper.map(author1,AuthorResponse.class)).collect(Collectors.toList());

        }throw new NotFoundExeption("Not found");
    }
}

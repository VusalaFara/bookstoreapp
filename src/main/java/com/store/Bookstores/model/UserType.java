package com.store.Bookstores.model;

public enum UserType {
    AUTHOR,
    STUDENT
}

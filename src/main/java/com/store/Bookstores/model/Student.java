package com.store.Bookstores.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@Entity
@Table(name = "student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    Long age;
    String password;
    String email;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "student_authority",
    joinColumns = @JoinColumn(name = "student_id"),
    inverseJoinColumns = @JoinColumn(name = "authority_id"))
    List<Authority>authoritiesList;

    @ManyToMany
    @JoinTable(name = "student_book",
    joinColumns = @JoinColumn (name = "student_id"),
    inverseJoinColumns = @JoinColumn(name = "book_id"))
    @JsonIgnore
    @ToString.Exclude
    List<Book>books;


    @ManyToMany
    @JoinTable(name = "student_author",
    joinColumns = @JoinColumn(name = "student_id"),
    inverseJoinColumns = @JoinColumn(name = "author_id"))
    @JsonIgnore
    @ToString.Exclude
    List<Author>authors;
}

package com.store.Bookstores.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@Entity
public class Authority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Enumerated(EnumType.STRING)
    Authorities authorities;

    @ManyToMany(mappedBy = "authoritiesList")
    @JsonIgnore
    @ToString.Exclude
    List<Student> studentRespons;

    @ManyToMany(mappedBy = "authoritiesList")
    @JsonIgnore
    @ToString.Exclude
    List<Author>authors;

    @ManyToMany(mappedBy = "authorityList")
    @JsonIgnore
    @ToString.Exclude
    List<AuthUser> authUsers;
}

package com.store.Bookstores.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "auth_user")
public class AuthUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;

    String password;

    String email;

    @Enumerated(EnumType.STRING)
    UserType userType;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "auth_user_authority",
    joinColumns = @JoinColumn(name = "auth_user_id"),
    inverseJoinColumns = @JoinColumn(name = "authority_id"))
    List<Authority>authorityList;
}

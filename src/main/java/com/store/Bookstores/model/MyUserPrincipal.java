package com.store.Bookstores.model;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@Builder
@RequiredArgsConstructor
public class MyUserPrincipal implements UserDetails {

    private final AuthUser authUser;


@Override
public Collection<? extends GrantedAuthority> getAuthorities() {
    // Convert the user's authorities to GrantedAuthority objects
    Set<GrantedAuthority> authorities = authUser.getAuthorityList().stream()
            .map(authority -> new SimpleGrantedAuthority(authority.getAuthorities().name()))
            .collect(Collectors.toSet());
    return authorities;
}

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}

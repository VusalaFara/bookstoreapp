package com.store.Bookstores.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;
    @Column(name = "book_name")
    String name;

//    @ManyToMany(mappedBy = "books")
//    @JsonIgnore
//    @ToString.Exclude
//    List<Author> authors;

     @ManyToOne()
     @JoinColumn(name = "author_id",referencedColumnName = "id")
     @JsonIgnore
    Author author;

    @ManyToMany(mappedBy = "books")
    @JsonIgnore
    @ToString.Exclude
    List<Student> students;
}

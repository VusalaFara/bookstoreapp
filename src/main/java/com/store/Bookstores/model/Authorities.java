package com.store.Bookstores.model;

public enum Authorities {

    PUBLIC,
    USER,
    ADMIN
}

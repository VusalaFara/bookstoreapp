package com.store.Bookstores.model;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@Entity
@Table(name = "author")
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Column(name = "name")
    String name;

    @Column(name = "email",unique = true)
    String email;

    @Column(name = "age")
    Long age;

    String password;


    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinTable(name = "author_authority",
    joinColumns= @JoinColumn(name = "author_id"),
    inverseJoinColumns = @JoinColumn( name = "authority_id"))
    List<Authority>authoritiesList;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER, mappedBy = "author")
    @JsonIgnore
    @ToString.Exclude
    List<Book>books;


    @ManyToMany(mappedBy = "authors",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JsonIgnore
    @ToString.Exclude
    List<Student> students;
}
